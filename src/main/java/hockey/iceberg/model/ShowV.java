package hockey.iceberg.model;

/**
 * Created by sievmi on 18.05.17.
 */
public class ShowV {
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    private String username;
    private String status;
}
