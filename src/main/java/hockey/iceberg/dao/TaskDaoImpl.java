package hockey.iceberg.dao;

import java.util.List;

import hockey.iceberg.model.Task;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

@Repository("employeeDao")
public class TaskDaoImpl extends AbstractDao<Integer, Task> implements TaskDao {



	public void deleteTask(int id) {
		Query query = getSession().createSQLQuery("delete from TASK where id = :id");
		query.setInteger("id", id);
		query.executeUpdate();
	}

	public void saveTask(Task task) {
		persist(task);
	}

	public List<Task> findAllTasks(String username, String status) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("name", username));
		criteria.add(Restrictions.eq("status", status));
		return (List<Task>) criteria.list();
	}

}
