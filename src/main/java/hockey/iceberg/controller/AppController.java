package hockey.iceberg.controller;

import java.util.List;

import javax.validation.Valid;

import hockey.iceberg.model.ShowV;
import hockey.iceberg.model.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import hockey.iceberg.service.TaskService;

@Controller
@RequestMapping("/")
public class AppController {

	@Autowired
	TaskService service;
	
	@Autowired
	MessageSource messageSource;

	@RequestMapping(value = {"/"}, method = RequestMethod.GET)
	public String index(ModelMap model) {
		ShowV userV = new ShowV();
		model.addAttribute("user", userV);
		return "index";
	}

	@RequestMapping(value = {"/show"}, method = RequestMethod.GET)
	public String show(ModelMap model) {
		ShowV userV = new ShowV();
		model.addAttribute("user", userV);
		return "show";
	}

	@RequestMapping(value = {"/show"}, method = RequestMethod.POST)
	public String show(@Valid ShowV showV, BindingResult result,
					   ModelMap model) {
		List<Task> tasks = service.findAllTasks(showV.getUsername(), showV.getStatus());
		model.addAttribute("tasks", tasks);

		return "list";
	}

	@RequestMapping(value = {"/list" }, method = RequestMethod.GET)
	public String listEmployees(ModelMap model) {

		List<Task> tasks = service.findAllTasks("sievmi", "actual");
		model.addAttribute("tasks", tasks);
		return "list";
	}

	@RequestMapping(value = { "/new" }, method = RequestMethod.GET)
	public String newEmployee(ModelMap model) {
		Task task = new Task();
		model.addAttribute("task", task);
		model.addAttribute("edit", false);
		return "registration";
	}

	@RequestMapping(value = { "/new" }, method = RequestMethod.POST)
	public String saveEmployee(@Valid Task task, BindingResult result,
			ModelMap model) {

		if (result.hasErrors()) {
			return "registration";
		}
		
		service.saveTask(task);

		model.addAttribute("success", "Task " + task.getName()+" "+task.getDescription() + " "+ task.getStatus()
				+ " registered successfully");
		return "success";
	}


	@RequestMapping(value = { "/delete" }, method = RequestMethod.GET)
	public String deleteTask(ModelMap model) {
		Task task = new Task();
		model.addAttribute("task", task);
		model.addAttribute("edit", false);
		return "delete";
	}

	@RequestMapping(value = { "/delete" }, method = RequestMethod.POST)
	public String deleteTask(@Valid Task task, BindingResult result,
							 ModelMap model) {
		service.deleteTask(task.getId());
		return "redirect:/list";
	}

}
