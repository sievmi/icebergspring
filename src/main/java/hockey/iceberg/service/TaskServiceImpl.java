package hockey.iceberg.service;

import java.util.List;

import hockey.iceberg.model.Task;
import hockey.iceberg.dao.TaskDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("employeeService")
@Transactional
public class TaskServiceImpl implements TaskService {

	@Autowired
	private TaskDao dao;

	public void saveTask(Task task) {
		dao.saveTask(task);
	}

	public void deleteTask(int taskID) {
		dao.deleteTask(taskID);
	}
	
	public List<Task> findAllTasks(String username, String status) {
		return dao.findAllTasks(username, status);
	}
	
}
