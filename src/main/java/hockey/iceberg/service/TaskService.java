package hockey.iceberg.service;

import java.util.List;

import hockey.iceberg.model.Task;

public interface TaskService {

	void saveTask(Task task);


	void deleteTask(int taskID);

	List<Task> findAllTasks(String username, String status);
}
