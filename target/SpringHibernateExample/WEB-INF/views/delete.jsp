<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
    <title>Task tracker</title>


</head>

<body>
<a href="/show">Show tasks</a> |
<a href="/new">Add task</a> |
<a href="/delete">Delete task</a>


<h2>Delete form</h2>

<form:form method="POST" modelAttribute="task">
    <table>
        <tr>
            <td><label for="id">Name: </label> </td>
            <td><form:input path="name" id="name"/></td>
        </tr>

        <tr>
            <td><label for="id">ID: </label> </td>
            <td><form:input path="id" id="id"/></td>
        </tr>

        <tr>
            <td colspan="3">
                <c:choose>
                    <c:when test="${edit}">
                        <input type="submit" value="Update"/>
                    </c:when>
                    <c:otherwise>
                        <input type="submit" value="Delete"/>
                    </c:otherwise>
                </c:choose>
            </td>
        </tr>
    </table>
</form:form>
<br/>
<br/>
</body>
</html>