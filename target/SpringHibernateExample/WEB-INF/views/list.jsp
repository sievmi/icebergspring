<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<title>Task Tracker</title>

	<style>
		tr:first-child{
			font-weight: bold;
			background-color: #C6C9C4;
		}
	</style>

</head>


<body>
	<h2>List of Tasks</h2>
	<table>
		<tr>
			<td>ID</td><td>Owner</td><td>Description</td><td>Status</td><td></td>
		</tr>
		<c:forEach items="${tasks}" var="task">
			<tr>

			<td>${task.id}</td><td>${task.name}</td> <td>${task.description}</td> <td>${task.status}</td>
			</tr>
			<tr>
		</c:forEach>
	</table>
	<br/>
	<a href="<c:url value='/new' />">Add New Task</a>
	<a href="<c:url value='/delete' />">Delete task</a>
	<a href="<c:url value='/show' />">Show task</a>
</body>
</html>